<?php

namespace Drupal\foundation_utility\Plugin\Filter;

use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Foundation Utility: Table Filter.
 *
 * @Filter(
 *   id = "foundation_utility_table_filter",
 *   title = @Translation("Foundation Utility: Table Filter"),
 *   description = @Translation("This filter allows you to add basic foundation classes to tables and remove width, height and style attributes from tables and table elements."),
 *   settings = {
 *     "table_add_scroll" = TRUE,
 *     "table_remove_width_height" = TRUE,
 *     "table_remove_style" = FALSE,
 *     "table_add_hover" = FALSE,
 *     "table_add_unstriped" = FALSE,
 *     "table_add_stack" = FALSE,
 *   },
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE
 * )
 */
class TableFilter extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form['table_add_scroll'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add <code>.scroll</code> class to tables'),
      '#default_value' => $this->settings['table_add_scroll'],
      '#description' => $this->t('Adds <code>.scroll</code> to enable horizontal scrolling to the tables. For better responsive table display.'),
    ];
    $form['table_remove_width_height'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Remove <code>width</code> and <code>height</code> attributes from tables and table cells. Typically used with .scroll class for responsive display.'),
      '#default_value' => $this->settings['table_remove_width_height'],
    ];
    $form['table_remove_style'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Removes <code>style</code> attribute from tables and table cells. Typically used with .scroll class for responsive display.'),
      '#default_value' => $this->settings['table_remove_style'],
    ];
    $form['table_add_hover'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add <code>.hover</code> class to tables'),
      '#default_value' => $this->settings['table_add_hover'],
      '#description' => $this->t('Adds <code>.hover</code> to lightly darken the tables rows on hover.'),
    ];
    $form['table_add_unstriped'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add <code>.unstriped</code> class to tables'),
      '#default_value' => $this->settings['table_add_unstriped'],
      '#description' => $this->t('Adds <code>.unstriped</code> to remove the stripes of the tables.'),
    ];
    $form['table_add_stack'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add <code>.stack</code> class to tables'),
      '#default_value' => $this->settings['table_add_stack'],
      '#description' => $this->t('Adds <code>.stack</code> to stack tables on small screens.'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $text = $this->filterText($text);
    $result = new FilterProcessResult($text);
    return $result;
  }

  /**
   * Filters table dom elements.
   *
   * @param string $text
   *   The WYSIWYG text.
   */
  protected function filterText(string $text) {
    $crawler = new Crawler($text);
    $filteredCrawler = $crawler->filter('table');
    if ($filteredCrawler->count() <= 0) {
      return $text;
    }
    /**
     * @var \DOMElement $domElement
     */
    foreach ($filteredCrawler as $domElement) {
      if ($this->settings['table_remove_width_height'] || $this->settings['table_remove_style']) {
        if ($this->settings['table_remove_width_height']) {
          $domElement->removeAttribute('width');
          $domElement->removeAttribute('height');
        }
        if ($this->settings['table_remove_style']) {
          $domElement->removeAttribute('style');
        }
        $this->removeWidthHeightChildrenRecursively($domElement);
      }
      // Get exsisting classes:
      $class = $domElement->getAttribute('class');
      if ($this->settings['table_add_hover']) {
        // Append hover class:
        $class .= ' hover';
      }
      if ($this->settings['table_add_unstriped']) {
        $class .= ' unstriped';
      }
      if ($this->settings['table_add_stack']) {
        $class .= ' stack';
      }
      if ($this->settings['table_add_scroll']) {
        $class .= ' scroll';
      }
      // Add modified classes, if changed:
      if ($class != $domElement->getAttribute('class')) {
        $domElement->setAttribute('class', $class);
      }
    }
    return $crawler->html();
  }

  /**
   * Removes the all children nodes of a DOMElement recursively.
   */
  protected function removeWidthHeightChildrenRecursively($domElement) {
    if (empty($domElement->childElementCount)) {
      return;
    }
    else {
      $childNodes = $domElement->childNodes;
      /**
       * @var \DOMElement $node
       */
      foreach ($childNodes as $childNode) {
        if (get_class($childNode) != 'DOMText') {
          if ($this->settings['table_remove_width_height']) {
            $childNode->removeAttribute('width');
            $childNode->removeAttribute('height');
          }
          if ($this->settings['table_remove_style']) {
            $childNode->removeAttribute('style');
          }
          $this->removeWidthHeightChildrenRecursively($childNode);
        }
      }
    }
  }

}
