<?php

namespace Drupal\Tests\foundation_utility\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Provides methods specifically for testing foundation_utility filters.
 *
 * @group foundation_utility
 */
class FoundationUtilityFiltersFunctionalTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'test_page_test',
    'foundation_utility',
    'filter_test',
  ];

  /**
   * A user with authenticated permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $user;

  /**
   * A user with admin permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->createContentType(['type' => 'article']);
    $this->config('system.site')->set('page.front', '/test-page')->save();
    $this->user = $this->drupalCreateUser([]);
    $this->adminUser = $this->drupalCreateUser([]);
    $this->adminUser->addRole($this->createAdminRole('admin', 'admin'));
    $this->adminUser->save();
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Tests if the module installation, won't break the site.
   */
  public function testInstallation() {
    $session = $this->assertSession();
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
  }

  /**
   * Tests if uninstalling the module, won't break the site.
   */
  public function testUninstallation() {
    // Go to uninstallation page an uninstall foundation_utility:
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    $this->drupalGet('/admin/modules/uninstall');
    $session->statusCodeEquals(200);
    $page->checkField('edit-uninstall-foundation-utility');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    // Confirm deinstall:
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    $session->pageTextContains('The selected modules have been uninstalled.');
  }

  /**
   * Tests removing width and height via filter setting.
   */
  public function testRemoveWidthHeight() {
    $session = $this->assertSession();
    // Edit our test filter format with our custom filter enabled:
    $edit = [
      'edit-filters-filter-html-escape-status' => 0,
      'edit-filters-foundation-utility-table-filter-status' => 1,
      'edit-filters-foundation-utility-table-filter-settings-table-remove-width-height' => 1,
    ];
    $this->drupalGet('admin/config/content/formats/manage/filter_test');
    $this->submitForm($edit, 'Save configuration');

    // Test if the filter settings form still works (#3301096)
    $this->drupalGet('admin/config/content/formats/manage/filter_test');
    $session->statusCodeEquals(200);

    // Create a node with a table:
    $node = $this->createNode([
      'type' => 'article',
      'title' => 'test123',
      'body' => [
        'value' => '
      <table id="test-table" width="40" height="40">
        <thead id="test-thead" width="40" height="40">
          <tr id="test-tr" width="40" height="40">
            <th id="test-th" width="40" height="40">header1</th>
            <th>header2</th>
            <th>header3</th>
          </tr>
        </thead>
        <tbody id="test-tbody" width="40" height="40">
          <tr>
            <td id="test-td" width="40" height="40">text1.1</td>
            <td>text1.2</td>
            <td>text1.3</td>
          </tr>
          <tr>
            <td>text2.1</td>
            <td>text2.2</td>
            <td>text2.3</td>
          </tr>
          <tr>
            <td>text3.1</td>
            <td>text3.2</td>
            <td>text3.3</td>
          </tr>
        </tbody>
      </table>',
        'format' => 'filter_test',
      ],
    ]);

    // Go to the created node and check if the elements do not have
    // width and height anymore:
    $this->drupalGet('/node/' . $node->id());
    $session->statusCodeEquals(200);

    $session->elementExists('css', 'table#test-table');
    // Check table:
    $session->elementAttributeNotExists('css', '#test-table', 'width');
    $session->elementAttributeNotExists('css', '#test-table', 'height');
    // Check thead:
    $session->elementAttributeNotExists('css', '#test-thead', 'width');
    $session->elementAttributeNotExists('css', '#test-thead', 'height');
    // Check tr:
    $session->elementAttributeNotExists('css', '#test-tr', 'width');
    $session->elementAttributeNotExists('css', '#test-tr', 'height');
    // Check th:
    $session->elementAttributeNotExists('css', '#test-th', 'width');
    $session->elementAttributeNotExists('css', '#test-th', 'height');
    // Check tbody:
    $session->elementAttributeNotExists('css', '#test-tbody', 'width');
    $session->elementAttributeNotExists('css', '#test-tbody', 'height');
    // Check td:
    $session->elementAttributeNotExists('css', '#test-td', 'width');
    $session->elementAttributeNotExists('css', '#test-td', 'height');
  }

  /**
   * Tests removing style via filter setting.
   */
  public function testRemoveStyle() {
    $session = $this->assertSession();
    // Edit our test filter format with our custom filter enabled:
    $edit = [
      'edit-filters-filter-html-escape-status' => 0,
      'edit-filters-foundation-utility-table-filter-status' => 1,
      'edit-filters-foundation-utility-table-filter-settings-table-remove-style' => 1,
    ];
    $this->drupalGet('admin/config/content/formats/manage/filter_test');
    $this->submitForm($edit, 'Save configuration');

    // Test if the filter settings form still works (#3301096)
    $this->drupalGet('admin/config/content/formats/manage/filter_test');
    $session->statusCodeEquals(200);

    // Create a node with a table:
    $node = $this->createNode([
      'type' => 'article',
      'title' => 'test123',
      'body' => [
        'value' => '
      <table id="test-table" style="bla.css">
        <thead id="test-thead" style="bla.css">
          <tr id="test-tr" style="bla.css">
            <th id="test-th" style="bla.css">header1</th>
            <th>header2</th>
            <th>header3</th>
          </tr>
        </thead>
        <tbody id="test-tbody" style="bla.css">
          <tr>
            <td id="test-td" style="bla.css">text1.1</td>
            <td>text1.2</td>
            <td>text1.3</td>
          </tr>
          <tr>
            <td>text2.1</td>
            <td>text2.2</td>
            <td>text2.3</td>
          </tr>
          <tr>
            <td>text3.1</td>
            <td>text3.2</td>
            <td>text3.3</td>
          </tr>
        </tbody>
      </table>',
        'format' => 'filter_test',
      ],
    ]);
    // Go to the created node and check if the elements do not have
    // style anymore:
    $this->drupalGet('/node/' . $node->id());
    $session->statusCodeEquals(200);
    $session->elementExists('css', 'table#test-table');
    // Check table:
    $session->elementAttributeNotExists('css', '#test-table', 'style');
    // Check thead:
    $session->elementAttributeNotExists('css', '#test-thead', 'style');
    // Check tr:
    $session->elementAttributeNotExists('css', '#test-tr', 'style');
    // Check th:
    $session->elementAttributeNotExists('css', '#test-th', 'style');
    // Check tbody:
    $session->elementAttributeNotExists('css', '#test-tbody', 'style');
    // Check td:
    $session->elementAttributeNotExists('css', '#test-td', 'style');
  }

  /**
   * Tests adding hover class via filter setting.
   */
  public function testAddHoverClass() {
    $session = $this->assertSession();
    // Edit our test filter format with our custom filter enabled:
    $edit = [
      'edit-filters-filter-html-escape-status' => 0,
      'edit-filters-foundation-utility-table-filter-status' => 1,
      'edit-filters-foundation-utility-table-filter-settings-table-add-hover' => 1,
    ];
    $this->drupalGet('admin/config/content/formats/manage/filter_test');
    $this->submitForm($edit, 'Save configuration');

    // Test if the filter settings form still works (#3301096)
    $this->drupalGet('admin/config/content/formats/manage/filter_test');
    $session->statusCodeEquals(200);

    // Create a node with a table:
    $node = $this->createNode([
      'type' => 'article',
      'title' => 'test123',
      'body' => [
        'value' => '
      <table id="test-table" class="test-class">
        <thead>
          <tr>
            <th>header1</th>
            <th>header2</th>
            <th>header3</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>text1.1</td>
            <td>text1.2</td>
            <td>text1.3</td>
          </tr>
          <tr>
            <td>text2.1</td>
            <td>text2.2</td>
            <td>text2.3</td>
          </tr>
          <tr>
            <td>text3.1</td>
            <td>text3.2</td>
            <td>text3.3</td>
          </tr>
        </tbody>
      </table>',
        'format' => 'filter_test',
      ],
    ]);
    $this->drupalGet('/node/' . $node->id());
    $session->statusCodeEquals(200);
    $session->elementExists('css', 'table#test-table');
    // Check table:
    $session->elementAttributeContains('css', '#test-table', 'class', 'hover');
    $session->elementAttributeContains('css', '#test-table', 'class', 'test-class');
  }

  /**
   * Tests adding unstriped class via filter setting.
   */
  public function testAddUnstripedClass() {
    $session = $this->assertSession();
    // Edit our test filter format with our custom filter enabled:
    $edit = [
      'edit-filters-filter-html-escape-status' => 0,
      'edit-filters-foundation-utility-table-filter-status' => 1,
      'edit-filters-foundation-utility-table-filter-settings-table-add-unstriped' => 1,
    ];
    $this->drupalGet('admin/config/content/formats/manage/filter_test');
    $this->submitForm($edit, 'Save configuration');

    // Test if the filter settings form still works (#3301096)
    $this->drupalGet('admin/config/content/formats/manage/filter_test');
    $session->statusCodeEquals(200);

    // Create a node with a table:
    $node = $this->createNode([
      'type' => 'article',
      'title' => 'test123',
      'body' => [
        'value' => '
      <table id="test-table" class="test-class">
        <thead>
          <tr>
            <th>header1</th>
            <th>header2</th>
            <th>header3</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>text1.1</td>
            <td>text1.2</td>
            <td>text1.3</td>
          </tr>
          <tr>
            <td>text2.1</td>
            <td>text2.2</td>
            <td>text2.3</td>
          </tr>
          <tr>
            <td>text3.1</td>
            <td>text3.2</td>
            <td>text3.3</td>
          </tr>
        </tbody>
      </table>',
        'format' => 'filter_test',
      ],
    ]);
    $this->drupalGet('/node/' . $node->id());
    $session->statusCodeEquals(200);
    $session->elementExists('css', 'table#test-table');
    // Check table:
    $session->elementAttributeContains('css', '#test-table', 'class', 'unstriped');
    $session->elementAttributeContains('css', '#test-table', 'class', 'test-class');
  }

  /**
   * Tests adding stack class via filter setting.
   */
  public function testAddStackClass() {
    $session = $this->assertSession();
    // Edit our test filter format with our custom filter enabled:
    $edit = [
      'edit-filters-filter-html-escape-status' => 0,
      'edit-filters-foundation-utility-table-filter-status' => 1,
      'edit-filters-foundation-utility-table-filter-settings-table-add-stack' => 1,
    ];
    $this->drupalGet('admin/config/content/formats/manage/filter_test');
    $this->submitForm($edit, 'Save configuration');

    // Test if the filter settings form still works (#3301096)
    $this->drupalGet('admin/config/content/formats/manage/filter_test');
    $session->statusCodeEquals(200);

    // Create a node with a table:
    $node = $this->createNode([
      'type' => 'article',
      'title' => 'test123',
      'body' => [
        'value' => '
      <table id="test-table" class="test-class">
        <thead>
          <tr>
            <th>header1</th>
            <th>header2</th>
            <th>header3</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>text1.1</td>
            <td>text1.2</td>
            <td>text1.3</td>
          </tr>
          <tr>
            <td>text2.1</td>
            <td>text2.2</td>
            <td>text2.3</td>
          </tr>
          <tr>
            <td>text3.1</td>
            <td>text3.2</td>
            <td>text3.3</td>
          </tr>
        </tbody>
      </table>',
        'format' => 'filter_test',
      ],
    ]);
    $this->drupalGet('/node/' . $node->id());
    $session->statusCodeEquals(200);
    $session->elementExists('css', 'table#test-table');
    // Check table:
    $session->elementAttributeContains('css', '#test-table', 'class', 'stack');
    $session->elementAttributeContains('css', '#test-table', 'class', 'test-class');
  }

  /**
   * Tests adding scroll class via filter setting.
   */
  public function testAddScrollClass() {
    $session = $this->assertSession();
    // Edit our test filter format with our custom filter enabled:
    $edit = [
      'edit-filters-filter-html-escape-status' => 0,
      'edit-filters-foundation-utility-table-filter-status' => 1,
      'edit-filters-foundation-utility-table-filter-settings-table-add-scroll' => 1,
    ];
    $this->drupalGet('admin/config/content/formats/manage/filter_test');
    $this->submitForm($edit, 'Save configuration');

    // Test if the filter settings form still works (#3301096)
    $this->drupalGet('admin/config/content/formats/manage/filter_test');
    $session->statusCodeEquals(200);

    // Create a node with a table:
    $node = $this->createNode([
      'type' => 'article',
      'title' => 'test123',
      'body' => [
        'value' => '
      <table id="test-table" class="test-class">
        <thead>
          <tr>
            <th>header1</th>
            <th>header2</th>
            <th>header3</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>text1.1</td>
            <td>text1.2</td>
            <td>text1.3</td>
          </tr>
          <tr>
            <td>text2.1</td>
            <td>text2.2</td>
            <td>text2.3</td>
          </tr>
          <tr>
            <td>text3.1</td>
            <td>text3.2</td>
            <td>text3.3</td>
          </tr>
        </tbody>
      </table>',
        'format' => 'filter_test',
      ],
    ]);
    $this->drupalGet('/node/' . $node->id());
    $session->statusCodeEquals(200);
    $session->elementExists('css', 'table#test-table');
    // Check table:
    $session->elementAttributeContains('css', '#test-table', 'class', 'scroll');
    $session->elementAttributeContains('css', '#test-table', 'class', 'test-class');
  }

  /**
   * Tests removing width, height and style and add all classes.
   */
  public function testRemoveAndAddAll() {
    $session = $this->assertSession();
    // Edit our test filter format with our custom filter enabled:
    $edit = [
      'edit-filters-filter-html-escape-status' => 0,
      'edit-filters-foundation-utility-table-filter-status' => 1,
      'edit-filters-foundation-utility-table-filter-settings-table-remove-width-height' => 1,
      'edit-filters-foundation-utility-table-filter-settings-table-remove-style' => 1,
      'edit-filters-foundation-utility-table-filter-settings-table-add-hover' => 1,
      'edit-filters-foundation-utility-table-filter-settings-table-add-unstriped' => 1,
      'edit-filters-foundation-utility-table-filter-settings-table-add-stack' => 1,
      'edit-filters-foundation-utility-table-filter-settings-table-add-scroll' => 1,
    ];
    $this->drupalGet('admin/config/content/formats/manage/filter_test');
    $this->submitForm($edit, 'Save configuration');

    // Test if the filter settings form still works (#3301096)
    $this->drupalGet('admin/config/content/formats/manage/filter_test');
    $session->statusCodeEquals(200);

    // Create a node with a table:
    $node = $this->createNode([
      'type' => 'article',
      'title' => 'test123',
      'body' => [
        'value' => '
      <table id="test-table" class="test-class" width="40" height="40" style="bla.css">
        <thead id="test-thead" width="40" height="40" style="bla.css">
          <tr id="test-tr" width="40" height="40" style="bla.css">
            <th id="test-th" width="40" height="40" style="bla.css">header1</th>
            <th>header2</th>
            <th>header3</th>
          </tr>
        </thead>
        <tbody id="test-tbody" width="40" height="40" style="bla.css">
          <tr>
            <td id="test-td" width="40" height="40" style="bla.css">text1.1</td>
            <td>text1.2</td>
            <td>text1.3</td>
          </tr>
          <tr>
            <td>text2.1</td>
            <td>text2.2</td>
            <td>text2.3</td>
          </tr>
          <tr>
            <td>text3.1</td>
            <td>text3.2</td>
            <td>text3.3</td>
          </tr>
        </tbody>
      </table>',
        'format' => 'filter_test',
      ],
    ]);
    // Go to the created node and check if the elements do not have
    // width and height anymore:
    $this->drupalGet('/node/' . $node->id());
    $session->statusCodeEquals(200);
    $session->elementExists('css', 'table#test-table');
    // Check table:
    $session->elementAttributeNotExists('css', '#test-table', 'width');
    $session->elementAttributeNotExists('css', '#test-table', 'height');
    // Check thead:
    $session->elementAttributeNotExists('css', '#test-thead', 'width');
    $session->elementAttributeNotExists('css', '#test-thead', 'height');
    // Check tr:
    $session->elementAttributeNotExists('css', '#test-tr', 'width');
    $session->elementAttributeNotExists('css', '#test-tr', 'height');
    // Check th:
    $session->elementAttributeNotExists('css', '#test-th', 'width');
    $session->elementAttributeNotExists('css', '#test-th', 'height');
    // Check tbody:
    $session->elementAttributeNotExists('css', '#test-tbody', 'width');
    $session->elementAttributeNotExists('css', '#test-tbody', 'height');
    // Check td:
    $session->elementAttributeNotExists('css', '#test-td', 'width');
    $session->elementAttributeNotExists('css', '#test-td', 'height');

    // Check if the elements do not have style anymore:
    // Check table:
    $session->elementAttributeNotExists('css', '#test-table', 'style');
    // Check thead:
    $session->elementAttributeNotExists('css', '#test-thead', 'style');
    // Check tr:
    $session->elementAttributeNotExists('css', '#test-tr', 'style');
    // Check th:
    $session->elementAttributeNotExists('css', '#test-th', 'style');
    // Check tbody:
    $session->elementAttributeNotExists('css', '#test-tbody', 'style');
    // Check td:
    $session->elementAttributeNotExists('css', '#test-td', 'style');

    // Check if classes are set on table:
    $session->elementAttributeContains('css', '#test-table', 'class', 'test-class');
    $session->elementAttributeContains('css', '#test-table', 'class', 'hover');
    $session->elementAttributeContains('css', '#test-table', 'class', 'unstriped');
    $session->elementAttributeContains('css', '#test-table', 'class', 'stack');
    $session->elementAttributeContains('css', '#test-table', 'class', 'scroll');
  }

}
